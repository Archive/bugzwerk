#include "config.h"

#include <libbugzwerk/bz.h>
#include <glib/gi18n.h>

typedef struct {
  char       **bug_numbers;
  GMainLoop   *loop;
} TestClosure;

static void
lookup_bug_cb (GObject      *object,
               GAsyncResult *result,
               gpointer      user_data)
{
  TestClosure    *closure = user_data;
  BzBugzilla     *bugzilla = BZ_BUGZILLA (object);
  GError         *error = NULL;
  BzBugzillaBug  *bug = NULL;

  bug = bz_bugzilla_lookup_bug_finish (bugzilla, result, &error);
  g_print ("%s: bug=%p\n", G_STRFUNC, bug);

  if (bug)
    {
      g_object_unref (bug);
    }

  if (error)
    {
      g_warning
        ("%s: %s(%d): %s", G_STRFUNC,
         g_quark_to_string (error->domain),
         error->code, error->message);

      g_error_free (error);
    }

  g_main_loop_quit (closure->loop);
}

static void
show_string (BzBugzillaInfo *info,
                  const char     *name)
{
  const char *value = bz_bugzilla_info_get_string (info, name);
  g_print ("%s: %s\n", name, value ? value : "");
}

static void
show_string_list (BzBugzillaInfo *info,
                  const char     *name)
{
  gpointer values = bz_bugzilla_info_get_string_list (info, name);
  char *strval;

  strval = g_strjoinv (", ", values);
  g_print ("%s: [%s]\n", name, strval);
  g_free (strval);
}

static void
query_info_cb (GObject      *object,
               GAsyncResult *result,
               gpointer      user_data)
{
  TestClosure    *closure = user_data;
  BzBugzilla     *bugzilla = BZ_BUGZILLA (object);
  GError         *error = NULL;
  BzBugzillaInfo *info = NULL;

  info = bz_bugzilla_query_info_finish (bugzilla, result, &error);

  if (info)
    {
      show_string (info, "bz:maintainer");

      show_string_list (info, "bz:priority");
      show_string_list (info, "bz:severity");
      show_string_list (info, "bz:resolution");
      show_string_list (info, "bz:status");

      g_object_unref (info);
    }

  if (error)
    {
      g_warning
        ("%s: %s(%d): %s", G_STRFUNC,
         g_quark_to_string (error->domain),
         error->code, error->message);

      g_error_free (error);
    }

  bz_bugzilla_lookup_bug_async
    (bugzilla, "123456", NULL, lookup_bug_cb, closure);
}

int
main (int   argc,
      char *argv[])
{
  char *default_bug_numbers[] = { "123456", NULL };
  char *base_uri = "http://bugzilla.gnome.org/";

  const GOptionEntry options[] = {
    { "base-uri", 'b', 0, G_OPTION_ARG_STRING, &base_uri,
      N_("Base URI of the Bugzilla instance"), N_("URI")
    },

    { 0, }
  };

  GOptionContext *cli;
  GError         *error = NULL;
  BzBugzilla     *bugzilla;
  TestClosure     closure;

  g_type_init ();

  cli = g_option_context_new (_("[BUG-NUMBER...]"));

  g_option_context_add_main_entries (cli, options, PACKAGE);

  if (!g_option_context_parse (cli, &argc, &argv, &error))
    {
      g_printerr ("option parsing failed: %s\n", error->message);
      g_option_context_free (cli);
      g_error_free (error);

      return 1;
    }

  g_option_context_free (cli);

  closure.bug_numbers = argc > 1 ? argv + 1 : default_bug_numbers;
  closure.loop = g_main_loop_new (NULL, FALSE);
  bugzilla = bz_bugzilla_new (base_uri);

  bz_bugzilla_query_info_async (bugzilla, NULL, query_info_cb, &closure);

  g_main_loop_run (closure.loop);
  g_main_loop_unref (closure.loop);

  g_object_unref (bugzilla);

  return 0;
}
