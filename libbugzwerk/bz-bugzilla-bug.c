#include "config.h"
#include "bz-bugzilla-bug.h"

G_DEFINE_TYPE (BzBugzillaBug, bz_bugzilla_bug, G_TYPE_OBJECT);

void
bz_bugzilla_bug_init (BzBugzillaBug *bug)
{
}

void
bz_bugzilla_bug_class_init (BzBugzillaBugClass *cls)
{
}

BzBugzillaBug *
bz_bugzilla_bug_new (void)
{
  return g_object_new (BZ_TYPE_BUGZILLA_BUG, NULL);
}

static void
bz_bugzilla_bug_start_element (GMarkupParseContext *context,
                               const char          *element_name,
                               const char          *attribute_names[],
                               const char          *attribute_values[],
                               gpointer             user_data,
                               GError             **error)
{
}

static void
bz_bugzilla_bug_end_element (GMarkupParseContext *context,
                             const char          *element_name,
                             gpointer             user_data,
                             GError             **error)
{
}

static void
bz_bugzilla_bug_text (GMarkupParseContext *context,
                      const char          *text,
                      gsize                text_len,
                      gpointer             user_data,
                      GError             **error)
{
}

BzBugzillaBug *
bz_bugzilla_bug_new_from_xml (const char *contents,
                              gssize      length,
                              GError    **error)
{
  static const GMarkupParser markup_handlers = {
    start_element: bz_bugzilla_bug_start_element,
    end_element: bz_bugzilla_bug_end_element,
    text: bz_bugzilla_bug_text,
  };

  GMarkupParseContext *context;
  BzBugzillaBug *bug;

  g_return_val_if_fail (NULL != contents, NULL);

  bug = g_object_new (BZ_TYPE_BUGZILLA_BUG, NULL);

  context = g_markup_parse_context_new
    (&markup_handlers, G_MARKUP_TREAT_CDATA_AS_TEXT |
     G_MARKUP_PREFIX_ERROR_POSITION, bug, NULL);

//g_print ("%s\n", contents);
  if (!g_markup_parse_context_parse (context, contents, length, error) ||
      !g_markup_parse_context_end_parse (context, error))
    {
      g_object_unref (bug);
      bug = NULL;
    }

  g_markup_parse_context_free (context);

  return bug;
}
