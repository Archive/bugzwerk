#include "config.h"
#include "bz-backlog-view.h"

#include <glib/gi18n.h>

#if 0
#define GET_PRIVATE(obj) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), \
   BZ_TYPE_BACKLOG_VIEW, BzBacklogViewPrivate))
#endif

#define DELEGATE(class, method, ...) G_STMT_START{ \
  if (class (bz_backlog_view_parent_class)->method) \
    class (bz_backlog_view_parent_class)->method (__VA_ARGS__); \
}G_STMT_END

enum {
  PROP_NONE,
  PROP_MODEL,
};

#if 0
typedef struct
{
  /* TODO: fill in private members */
} BzBacklogViewPrivate;
#endif

G_DEFINE_TYPE
  (BzBacklogView, bz_backlog_view,
   GTK_TYPE_TREE_VIEW);

static GtkListStore *
bz_text_store_new (const char *values[])
{
  GtkListStore *store;
  GtkTreeIter iter;
  unsigned i;
 
  store = gtk_list_store_new (1, G_TYPE_STRING);

  for (i = 0; values[i]; ++i)
    {
      gtk_list_store_append (store, &iter);
      gtk_list_store_set (store, &iter, 0, values[i], -1);
    }

  return store;
}

static void
bz_backlog_view_init (BzBacklogView *view)
{
}

static void
bz_backlog_view_constructed (GObject *object)
{
  const char *severities[] = { _("blocker"), _("critical"), _("major"), _("normal"), _("minor"), NULL };
  const char *priorities[] = { _("high"), _("medium"), _("low"), _("unspecified"), NULL };

  GtkTreeView *tree_view = GTK_TREE_VIEW (object);
  GtkTreeViewColumn *column;
  GtkCellRenderer *cell;

  cell = g_object_new
    (GTK_TYPE_CELL_RENDERER_TEXT, "xalign", 1.0, NULL);
  column = gtk_tree_view_column_new_with_attributes
    (_("Bug"), cell, "text", BZ_BACKLOG_MODEL_COLUMN_BUG, NULL);
  gtk_tree_view_column_set_sort_column_id
    (column, BZ_BACKLOG_MODEL_COLUMN_BUG);

  gtk_tree_view_append_column (tree_view, column);

  cell = g_object_new
    (GTK_TYPE_CELL_RENDERER_SPIN, "xalign", 1.0, NULL);
  column = gtk_tree_view_column_new_with_attributes
    (_("Effort"), cell, "text", BZ_BACKLOG_MODEL_COLUMN_EFFORT, NULL);
  gtk_tree_view_column_set_sort_column_id
    (column, BZ_BACKLOG_MODEL_COLUMN_EFFORT);

  gtk_tree_view_append_column (tree_view, column);

  cell = g_object_new
    (GTK_TYPE_CELL_RENDERER_COMBO,
     "xalign", 0.5, "editable", TRUE, "has-entry", FALSE,
     "model", bz_text_store_new (severities), "text-column", 0, NULL);
  column = gtk_tree_view_column_new_with_attributes
    (_("Severity"), cell, "text", BZ_BACKLOG_MODEL_COLUMN_SEVERITY, NULL);
  gtk_tree_view_column_set_sort_column_id
    (column, BZ_BACKLOG_MODEL_COLUMN_SEVERITY);

  gtk_tree_view_append_column (tree_view, column);

  cell = g_object_new
    (GTK_TYPE_CELL_RENDERER_COMBO,
     "xalign", 0.5, "editable", TRUE, "has-entry", FALSE,
     "model", bz_text_store_new (priorities), "text-column", 0, NULL);
  column = gtk_tree_view_column_new_with_attributes
    (_("Priority"), cell, "text", BZ_BACKLOG_MODEL_COLUMN_PRIORITY, NULL);
  gtk_tree_view_column_set_sort_column_id
    (column, BZ_BACKLOG_MODEL_COLUMN_PRIORITY);

  gtk_tree_view_append_column (tree_view, column);

  cell = g_object_new
    (GTK_TYPE_CELL_RENDERER_TEXT, "xalign", 0.5, NULL);
  column = gtk_tree_view_column_new_with_attributes
    (_("Keywords"), cell, "text", BZ_BACKLOG_MODEL_COLUMN_KEYWORDS, NULL);
  gtk_tree_view_column_set_sort_column_id
    (column, BZ_BACKLOG_MODEL_COLUMN_KEYWORDS);

  gtk_tree_view_append_column (tree_view, column);

  cell = g_object_new
    (GTK_TYPE_CELL_RENDERER_TEXT, "ellipsize", PANGO_ELLIPSIZE_END, NULL);
  column = gtk_tree_view_column_new_with_attributes
    (_("Description"), cell,
     "text", BZ_BACKLOG_MODEL_COLUMN_DESCRIPTION, NULL);
  gtk_tree_view_column_set_sort_column_id
    (column, BZ_BACKLOG_MODEL_COLUMN_DESCRIPTION);

  gtk_tree_view_column_set_expand (column, TRUE);
  gtk_tree_view_append_column (tree_view, column);
}

static void
bz_backlog_view_day_data_cb (GtkTreeViewColumn *column,
                             GtkCellRenderer   *cell,
                             GtkTreeModel      *model,
                             GtkTreeIter       *iter,
                             gpointer           data)
{
  int i = GPOINTER_TO_INT (data);
  BzBacklogDayArray *array = NULL;
  BzBacklogDay *day = NULL;

  gtk_tree_model_get (model, iter, BZ_BACKLOG_MODEL_COLUMN_DAYS, &array, -1);

  if (array)
    day = bz_backlog_day_array_get (array, i);

  if (day)
    {
      g_object_set
        (cell, "text", day->note,
         "cell-background-gdk", day->bg,
         "foreground-gdk", day->fg, NULL);
    }

  bz_backlog_day_array_free (array);
}

static void
bz_backlog_view_model_ready (BzBacklogModel *model,
                             gpointer        user_data)
{
  const char *states[] = {
    _("fixed"), _("active"), _("postponed"), _("assigned"),
    _("impeded"), _("review"), _("transfered"), NULL
  };

  BzBacklogView *view = user_data;
  GtkListStore *state_model;
  const char *const *days;
  gboolean visible;
  GList *columns;
  unsigned i;

  state_model = bz_text_store_new (states);
  columns = gtk_tree_view_get_columns (GTK_TREE_VIEW (view));

  for (i = 0; columns; ++i, columns = g_list_delete_link (columns, columns))
    {
      visible = bz_backlog_model_has_column (model, i);
      gtk_tree_view_column_set_visible (columns->data, visible);
    }

  days = bz_backlog_model_get_day_names (model);

  for (i = 0; days[i]; ++i)
    {
      GtkCellRenderer *cell;

      cell = g_object_new
        (GTK_TYPE_CELL_RENDERER_COMBO,
         "width-chars", 8, "editable", TRUE, "has-entry", FALSE, 
         "model", state_model, "text-column", 0, NULL),

      gtk_tree_view_insert_column_with_data_func
        (GTK_TREE_VIEW (view), BZ_BACKLOG_MODEL_COLUMN_BUG + i + 1, days[i],
         cell, bz_backlog_view_day_data_cb, GINT_TO_POINTER (i), NULL);
    }
}

static void
bz_backlog_view_model_failed (BzBacklogModel *model,
                              const GError   *error)
{
  g_print ("%s: %s\n", G_STRFUNC, error ? error->message : "???");
}

static void
bz_backlog_view_disconnect_model (GObject *object)
{
  GtkTreeModel *model;

  model = gtk_tree_view_get_model (GTK_TREE_VIEW (object));

  if (model)
    {
      g_signal_handlers_disconnect_by_func
        (model, bz_backlog_view_model_ready, object);
      g_signal_handlers_disconnect_by_func
        (model, bz_backlog_view_model_failed, object);
    }
}

static void
bz_backlog_view_connect_model (GObject *object)
{
  GtkTreeModel *model;

  model = gtk_tree_view_get_model (GTK_TREE_VIEW (object));

  if (model)
    {
      g_signal_connect
        (model, "ready",
         G_CALLBACK (bz_backlog_view_model_ready), object);
      g_signal_connect
        (model, "failed",
         G_CALLBACK (bz_backlog_view_model_failed), object);
    }
}

static void
bz_backlog_view_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  GtkTreeModel *model;

  switch (prop_id)
    {
      case PROP_MODEL:
        model = g_value_get_object (value);
        g_return_if_fail (BZ_IS_BACKLOG_MODEL (model));
        bz_backlog_view_disconnect_model (object);

        DELEGATE (G_OBJECT_CLASS, set_property, object, pspec->param_id, value, pspec);

        g_warning ("%s: FIXME: update columns if fully loaded", G_STRFUNC);
        g_warning ("%s: FIXME: reset day columns", G_STRFUNC);

        bz_backlog_view_connect_model (object);
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
bz_backlog_view_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  switch (prop_id)
    {
      case PROP_MODEL:
        DELEGATE (G_OBJECT_CLASS, get_property, object, pspec->param_id, value, pspec);
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
bz_backlog_view_dispose (GObject *object)
{
  bz_backlog_view_disconnect_model (object);
  DELEGATE (G_OBJECT_CLASS, dispose, object);
}


static void
bz_backlog_view_class_init (BzBacklogViewClass *cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);

  object_class->constructed  = bz_backlog_view_constructed;
  object_class->dispose      = bz_backlog_view_dispose;

  object_class->set_property = bz_backlog_view_set_property;
  object_class->get_property = bz_backlog_view_get_property;

  g_object_class_override_property (object_class, PROP_MODEL, "model");

#if 0 
  g_type_class_add_private (cls, sizeof (BzBacklogViewPrivate));
#endif
}

GtkWidget *
bz_backlog_view_new (void)
{
  BzBacklogModel *model;
  GtkWidget *view;

  model = bz_backlog_model_new ();
  view = bz_backlog_view_new_with_model (model);
  g_object_unref (model);

  return view;
}

GtkWidget *
bz_backlog_view_new_with_model (BzBacklogModel *model)
{
  g_return_val_if_fail (BZ_IS_BACKLOG_MODEL (model), NULL);
  return g_object_new (BZ_TYPE_BACKLOG_VIEW, "model", model, NULL);
}
