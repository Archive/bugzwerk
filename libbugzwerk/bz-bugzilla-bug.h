#ifndef __BZ_BUGZILLA_BUG_H__
#define __BZ_BUGZILLA_BUG_H__

#include <gio/gio.h>

G_BEGIN_DECLS

#define BZ_TYPE_BACKLOG_DAY_ARRAY \
  (bz_backlog_day_array_get_type ())

#define BZ_TYPE_BUGZILLA_BUG \
  (bz_bugzilla_bug_get_type ())
#define BZ_BUGZILLA_BUG(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
   BZ_TYPE_BUGZILLA_BUG, BzBugzillaBug))
#define BZ_BUGZILLA_BUG_CLASS(cls) \
  (G_TYPE_CHECK_CLASS_CAST ((cls), \
   BZ_TYPE_BUGZILLA_BUG, BzBugzillaBugClass))
#define BZ_IS_BUGZILLA_BUG(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
   BZ_TYPE_BUGZILLA_BUG))
#define BZ_IS_BUGZILLA_BUG_CLASS(obj) \
  (G_TYPE_CHECK_CLASS_TYPE ((obj), \
   BZ_TYPE_BUGZILLA_BUG))
#define BZ_BUGZILLA_BUG_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
   BZ_TYPE_BUGZILLA_BUG, BzBugzillaBugClass))

typedef struct _BzBugzillaBug      BzBugzillaBug;
typedef struct _BzBugzillaBugClass BzBugzillaBugClass;

struct _BzBugzillaBug {
  GObject parent_instance;
};

struct _BzBugzillaBugClass {
  GObjectClass parent_class;
};

GType
bz_bugzilla_bug_get_type     (void) G_GNUC_CONST;

BzBugzillaBug *
bz_bugzilla_bug_new          (void);

BzBugzillaBug *
bz_bugzilla_bug_new_from_xml (const char *contents,
                              gssize      length,
                              GError    **error);

G_END_DECLS

#endif /* __BZ_BUGZILLA_BUG_H__ */
