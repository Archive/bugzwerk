#include "config.h"
#include "bz-backlog-model.h"

#include <glib/gi18n.h>
#include <stdlib.h>
#include <string.h>

#define GET_PRIVATE(obj) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), \
   BZ_TYPE_BACKLOG_MODEL, BzBacklogModelPrivate))

#define DELEGATE(class, method, ...) G_STMT_START{ \
  if (class (bz_backlog_model_parent_class)->method) \
    class (bz_backlog_model_parent_class)->method (__VA_ARGS__); \
}G_STMT_END

enum {
  PROP_NONE,
  /* TODO: fill in properties */
};

enum {
  READY,
  FAILED,
  LAST_SIGNAL
};

typedef enum {
  BZ_BACKLOG_NODE_UNKNOWN,
  BZ_BACKLOG_NODE_SHEET,
  BZ_BACKLOG_NODE_STYLE_REGION,
  BZ_BACKLOG_NODE_STYLE,
  BZ_BACKLOG_NODE_CELL,
} BzBacklogNodeType;

typedef struct _BzBacklogNode   BzBacklogNode;
typedef struct _BzBacklogReader BzBacklogReader;

typedef BzBacklogNode *
  (* BzBacklogStartElementHandler) (BzBacklogReader  *reader,
                                    const char       *element_name,
                                    const char      **attribute_names,
                                    const char      **attribute_values,
                                    GError          **error);

typedef void
  (* BzBacklogEndElementHandler) (BzBacklogReader  *reader,
                                  const char       *element_name,
                                  GError          **error);

struct _BzBacklogNode
{
  BzBacklogNodeType  type;
  char              *name;
  unsigned           ref_count;

  union {
    struct {
      unsigned   n_columns;
      GPtrArray *styles;
      GPtrArray *cells;
    } sheet;

    struct {
      unsigned       start_row, end_row;
      unsigned       start_col, end_col;
      BzBacklogNode *style;
    } style_region;

    struct {
      GdkColor  fg;
      GdkColor  bg;
    } style;

    struct {
      unsigned       row, col;
      BzBacklogNode *style;
      char          *text;
    } cell;
  };
};

struct _BzBacklogReader
{
  BzBacklogModel      *model;

  GCancellable        *cancellable;
  char                 buffer[4096];

  GMarkupParseContext *parser;
  GHashTable          *start_element_handlers;
  GHashTable          *end_element_handlers;

  GSList              *node_stack;
  BzBacklogNode       *current_sheet;

  GPtrArray           *sheets;
};

typedef struct
{
  BzBacklogReader  *reader;
  unsigned          columns;
  char            **day_names;
} BzBacklogModelPrivate;

G_DEFINE_TYPE (BzBacklogModel, bz_backlog_model, GTK_TYPE_LIST_STORE);

static guint signals[LAST_SIGNAL] = { 0, };

BzBacklogDayArray *
bz_backlog_day_array_new (int size)
{
  BzBacklogDayArray *array;

  array = g_array_sized_new (FALSE, TRUE, sizeof (BzBacklogDay), size);
  array = g_array_set_size (array, size);

  return array;
}

BzBacklogDayArray *
bz_backlog_day_array_copy (BzBacklogDayArray *array)
{
  GArray *copy;
  int i;

  if (!array)
    return NULL;

  copy = bz_backlog_day_array_new (array->len);

  for (i = 0; i < array->len; ++i)
    {
      const BzBacklogDay *src;
      BzBacklogDay *dst;

      src = bz_backlog_day_array_get (array, i);
      dst = bz_backlog_day_array_get (copy, i);

      if (src->bg)
        dst->bg = gdk_color_copy (src->bg);
      if (src->fg)
        dst->fg = gdk_color_copy (src->fg);

      dst->note = g_strdup (src->note);
    }

  return copy;
}

void
bz_backlog_day_array_free (BzBacklogDayArray *array)
{
  BzBacklogDay *day;
  int i;

  if (!array)
    return;

  for (i = 0; i < array->len; ++i)
    {
      day = bz_backlog_day_array_get (array, i);

      if (day->bg)
        gdk_color_free (day->bg);
      if (day->fg)
        gdk_color_free (day->fg);

      g_free (day->note);
    }

  g_array_free (array, TRUE);
}

BzBacklogDay *
bz_backlog_day_array_get (BzBacklogDayArray *array,
                          unsigned           i)
{
  g_return_val_if_fail (NULL != array, NULL);
  return &g_array_index (array, BzBacklogDay, i);
}

GType
bz_backlog_day_array_get_type (void)
{
  static GType type = 0;

  if (G_UNLIKELY (!type))
    type = g_boxed_type_register_static
      ("BzBacklogDayArray",
       (GBoxedCopyFunc) bz_backlog_day_array_copy,
       (GBoxedFreeFunc) bz_backlog_day_array_free);

  return type;
}

static BzBacklogNode *
bz_backlog_node_new (BzBacklogNodeType  type,
                     const char        *name)
{
  BzBacklogNode *node;

  node = g_slice_new0 (BzBacklogNode);

  node->ref_count = 1;
  node->name = g_strdup (name);
  node->type = type;

  return node;
}

static BzBacklogNode *
bz_backlog_node_ref (BzBacklogNode *node)
{
  ++node->ref_count;
  return node;
}

static void
bz_backlog_node_array_free (GPtrArray *array);

static void
bz_backlog_node_unref (BzBacklogNode *node)
{
  if (!node || --node->ref_count > 0)
    return;

  g_free (node->name);

  switch (node->type)
    {
      case BZ_BACKLOG_NODE_SHEET:
        bz_backlog_node_array_free (node->sheet.styles);
        bz_backlog_node_array_free (node->sheet.cells);
        break;

      case BZ_BACKLOG_NODE_STYLE_REGION:
        bz_backlog_node_unref (node->style_region.style);
        break;

      case BZ_BACKLOG_NODE_CELL:
        bz_backlog_node_unref (node->cell.style);
        g_free (node->cell.text);
        break;

      case BZ_BACKLOG_NODE_STYLE:
      case BZ_BACKLOG_NODE_UNKNOWN:
        break;
    }

  g_slice_free (BzBacklogNode, node);
}

static void
bz_backlog_node_array_free (GPtrArray *array)
{
  if (array)
    {
      g_ptr_array_foreach (array, (GFunc) bz_backlog_node_unref, NULL);
      g_ptr_array_free (array, TRUE);
    }
}

static unsigned
bz_backlog_sheet_get_cell_index (const BzBacklogNode *node,
                                 unsigned             row,
                                 unsigned             col)
{
  return node->sheet.n_columns * row + col;
}

static BzBacklogNode *
bz_backlog_sheet_get_cell (const BzBacklogNode *node,
                           unsigned             row,
                           unsigned             col)
{
  unsigned i = bz_backlog_sheet_get_cell_index (node, row, col);

  if (col >= node->sheet.n_columns || i >= node->sheet.cells->len)
    return NULL;

  return node->sheet.cells->pdata[i];
}

static BzBacklogNode *
bz_backlog_sheet_create_cell (const BzBacklogNode *node,
                              unsigned             row,
                              unsigned             col)
{
  unsigned i = bz_backlog_sheet_get_cell_index (node, row, col);

  if (col >= node->sheet.n_columns || i >= node->sheet.cells->len)
    return NULL;

  if (!node->sheet.cells->pdata[i])
    node->sheet.cells->pdata[i] = bz_backlog_node_new (BZ_BACKLOG_NODE_CELL, NULL);

  return node->sheet.cells->pdata[i];
}

static BzBacklogNode *
bz_backlog_reader_start_sheet_element (BzBacklogReader  *reader,
                                       const char       *element_name,
                                       const char      **attribute_names,
                                       const char      **attribute_values,
                                       GError          **error)
{
  BzBacklogNode *node = NULL;

  if (reader->current_sheet)
    {
      g_set_error
        (error, G_MARKUP_ERROR,
         G_MARKUP_ERROR_INVALID_CONTENT,
         "Unexpected element: %s", element_name);

      return NULL;
    }

  node = bz_backlog_node_new (BZ_BACKLOG_NODE_SHEET, element_name);
  node->sheet.styles = g_ptr_array_new ();
  node->sheet.cells = g_ptr_array_new ();

  g_ptr_array_add (reader->sheets, bz_backlog_node_ref (node));
  reader->current_sheet = node;

  return node;
}

static int
bz_backlog_cell_compare_by_position (gconstpointer a,
                                     gconstpointer b)
{
  const BzBacklogNode *const *const node_ptr_a = a;
  const BzBacklogNode *const *const node_ptr_b = b;
  const BzBacklogNode *const node_a = *node_ptr_a;
  const BzBacklogNode *const node_b = *node_ptr_b;

  if (!node_a)
    return node_b ? 1 : 0;
  else if (!node_b)
    return -1;

  if (node_a->cell.row < node_b->cell.row)
    return -1;
  if (node_a->cell.row > node_b->cell.row)
    return +1;

  if (node_a->cell.col < node_b->cell.col)
    return -1;
  if (node_a->cell.col > node_b->cell.col)
    return +1;

  return 0;
}

static void
bz_backlog_reader_end_sheet_element (BzBacklogReader  *reader,
                                     const char       *element_name,
                                     GError          **error)
{
  unsigned maxrow = 0, maxcol = 0, i;
  GPtrArray *cells, *styles;

  g_return_if_fail (reader->node_stack);
  g_return_if_fail (reader->node_stack->data == reader->current_sheet);

  cells = reader->current_sheet->sheet.cells;
  styles = reader->current_sheet->sheet.styles;

  /* count rows and columns */

  for (i = 0; i < cells->len; ++i)
    {
      BzBacklogNode *node = cells->pdata[i];
      maxrow = MAX (maxrow, node->cell.row);
      maxcol = MAX (maxcol, node->cell.col);
    }

  reader->current_sheet->sheet.n_columns = maxcol + 1;

  /* place cells according to their position */

  g_ptr_array_sort (cells, bz_backlog_cell_compare_by_position);
  g_ptr_array_set_size (cells, (maxrow + 1) * (maxcol + 1));

  while (i-- > 0)
    {
      BzBacklogNode *node = cells->pdata[i];
      unsigned j;

      j = bz_backlog_sheet_get_cell_index
        (reader->current_sheet, node->cell.row, node->cell.col);

      cells->pdata[j] = cells->pdata[i];
      cells->pdata[i] = NULL;
    }

  /* apply styles */

  for (i = 0; i < styles->len; ++i)
    {
      BzBacklogNode *region = styles->pdata[i];
      BzBacklogNode *style;
      unsigned r, c;

      style = region->style_region.style;

      if (!style)
        continue;

      for (r = region->style_region.start_row; r <= region->style_region.end_row; ++r)
        for (c = region->style_region.start_col; c <= region->style_region.end_col; ++c)
          {
            BzBacklogNode *node = bz_backlog_sheet_create_cell (reader->current_sheet, r, c);

            if (!node)
              continue;

            if (node->cell.style)
              bz_backlog_node_unref (node->cell.style);

            node->cell.style = bz_backlog_node_ref (style);
          }
    }

  /* reset current sheet pointer */

  reader->current_sheet = NULL;
}

static BzBacklogNode *
bz_backlog_reader_start_style_region_element
                                      (BzBacklogReader  *reader,
                                       const char       *element_name,
                                       const char      **attribute_names,
                                       const char      **attribute_values,
                                       GError          **error)
{
  BzBacklogNode *node = NULL;

  const char *start_col, *end_col;
  const char *start_row, *end_row;

  if (!reader->current_sheet)
    {
      g_set_error
        (error, G_MARKUP_ERROR,
         G_MARKUP_ERROR_INVALID_CONTENT,
         "Unexpected element: %s", element_name);

      return NULL;
    }

  if (g_markup_collect_attributes
        (element_name, attribute_names, attribute_values, error,
         G_MARKUP_COLLECT_STRING, "startCol", &start_col,
         G_MARKUP_COLLECT_STRING, "startRow", &start_row,
         G_MARKUP_COLLECT_STRING, "endCol",   &end_col,
         G_MARKUP_COLLECT_STRING, "endRow",   &end_row,
         G_MARKUP_COLLECT_INVALID))
    {
      node = bz_backlog_node_new (BZ_BACKLOG_NODE_STYLE_REGION, element_name);

      node->style_region.start_col = atoi (start_col);
      node->style_region.start_row = atoi (start_row);

      node->style_region.end_col = atoi (end_col);
      node->style_region.end_row = atoi (end_row);

      g_ptr_array_add
        (reader->current_sheet->sheet.styles,
         bz_backlog_node_ref (node));
    }

  return node;
}

static BzBacklogNode *
bz_backlog_reader_start_style_element (BzBacklogReader  *reader,
                                       const char       *element_name,
                                       const char      **attribute_names,
                                       const char      **attribute_values,
                                       GError          **error)
{
  BzBacklogNode *node = NULL;
  BzBacklogNode *region = NULL;

  const char *fg, *bg, *skip;
  unsigned r, g, b;

  if (reader->node_stack)
    region = reader->node_stack->data;

  if (!region || BZ_BACKLOG_NODE_STYLE_REGION != region->type || region->style_region.style)
    {
      g_set_error
        (error, G_MARKUP_ERROR,
         G_MARKUP_ERROR_INVALID_CONTENT,
         "Unexpected element: %s", element_name);

      return NULL;
    }

  if (g_markup_collect_attributes
        (element_name, attribute_names, attribute_values, error,
         G_MARKUP_COLLECT_STRING, "Back",         &bg,
         G_MARKUP_COLLECT_STRING, "Fore",         &fg,
         G_MARKUP_COLLECT_STRING, "Format",       &skip,
         G_MARKUP_COLLECT_STRING, "HAlign",       &skip,
         G_MARKUP_COLLECT_STRING, "Hidden",       &skip,
         G_MARKUP_COLLECT_STRING, "Indent",       &skip,
         G_MARKUP_COLLECT_STRING, "Locked",       &skip,
         G_MARKUP_COLLECT_STRING, "PatternColor", &skip,
         G_MARKUP_COLLECT_STRING, "Rotation",     &skip,
         G_MARKUP_COLLECT_STRING, "Shade",        &skip,
         G_MARKUP_COLLECT_STRING, "ShrinkToFit",  &skip,
         G_MARKUP_COLLECT_STRING, "VAlign",       &skip,
         G_MARKUP_COLLECT_STRING, "WrapText",     &skip,
         G_MARKUP_COLLECT_INVALID))
    {
      node = bz_backlog_node_new (BZ_BACKLOG_NODE_STYLE, element_name);
      region->style_region.style = bz_backlog_node_ref (node);

      sscanf (fg, "%x:%x:%x", &r, &g, &b);

      node->style.fg.red   = r;
      node->style.fg.green = g;
      node->style.fg.blue  = b;
      node->style.fg.pixel = 0;

      sscanf (bg, "%x:%x:%x", &r, &g, &b);

      node->style.bg.red   = r;
      node->style.bg.green = g;
      node->style.bg.blue  = b;
      node->style.bg.pixel = 0;
    }

  return node;
}

static BzBacklogNode *
bz_backlog_reader_start_cell_element (BzBacklogReader  *reader,
                                      const char       *element_name,
                                      const char      **attribute_names,
                                      const char      **attribute_values,
                                      GError          **error)
{
  BzBacklogNode *node = NULL;

  const char *row, *col, *skip;

  if (!reader->current_sheet)
    {
      g_set_error
        (error, G_MARKUP_ERROR,
         G_MARKUP_ERROR_INVALID_CONTENT,
         "Unexpected element: %s", element_name);

      return NULL;
    }

  if (g_markup_collect_attributes
        (element_name, attribute_names, attribute_values, error,
         G_MARKUP_COLLECT_STRING, "Row",       &row,
         G_MARKUP_COLLECT_STRING, "Col",       &col,
         G_MARKUP_COLLECT_OPTIONAL |
         G_MARKUP_COLLECT_STRING, "ValueType", &skip,
         G_MARKUP_COLLECT_OPTIONAL |
         G_MARKUP_COLLECT_STRING, "ExprID",    &skip,
         G_MARKUP_COLLECT_INVALID))
    {
      node = bz_backlog_node_new (BZ_BACKLOG_NODE_CELL, element_name);

      node->cell.row        = atoi (row);
      node->cell.col        = atoi (col);

      g_ptr_array_add
        (reader->current_sheet->sheet.cells,
         bz_backlog_node_ref (node));
    }

  return node;
}

static BzBacklogNode *
bz_backlog_reader_start_document (BzBacklogReader  *reader,
                                  const char       *element_name,
                                  const char      **attribute_names,
                                  const char      **attribute_values,
                                  GError          **error)
{
  const char *xmlns;
  int i;

  reader->start_element_handlers =
    g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);
  reader->end_element_handlers =
    g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);

  for (i = 0; attribute_names[i]; ++i)
    {
      if (g_str_has_prefix (attribute_names[i], "xmlns:"))
        {
          xmlns = attribute_names[i] + strlen ("xmlns:");

          if (!strcmp (attribute_values[i], "http://www.gnumeric.org/v10.dtd"))
            {
              g_hash_table_insert
                (reader->start_element_handlers,
                 g_strdup_printf ("%s:Sheet", xmlns),
                 bz_backlog_reader_start_sheet_element);
              g_hash_table_insert
                (reader->end_element_handlers,
                 g_strdup_printf ("%s:Sheet", xmlns),
                 bz_backlog_reader_end_sheet_element);

              g_hash_table_insert
                (reader->start_element_handlers,
                 g_strdup_printf ("%s:StyleRegion", xmlns),
                 bz_backlog_reader_start_style_region_element);
              g_hash_table_insert
                (reader->start_element_handlers,
                 g_strdup_printf ("%s:Style", xmlns),
                 bz_backlog_reader_start_style_element);
              g_hash_table_insert
                (reader->start_element_handlers,
                 g_strdup_printf ("%s:Cell", xmlns),
                 bz_backlog_reader_start_cell_element);
            }
        }
    }

  return NULL;
}

static void
bz_backlog_reader_start_element (GMarkupParseContext *context,
                                 const char          *element_name,
                                 const char         **attribute_names,
                                 const char         **attribute_values,
                                 gpointer             user_data,
                                 GError             **error)
{
  BzBacklogReader *reader = user_data;
  BzBacklogStartElementHandler start_element;
  BzBacklogNode *node = NULL;

  if (reader->start_element_handlers)
    {
      start_element = g_hash_table_lookup (reader->start_element_handlers, element_name);

      if (start_element)
        node = start_element (reader, element_name, attribute_names, attribute_values, error);
      else
        node = bz_backlog_node_new (BZ_BACKLOG_NODE_UNKNOWN, element_name);

      if (node)
        reader->node_stack = g_slist_prepend (reader->node_stack, node);
    }
  else if (g_str_has_suffix (element_name, ":Workbook"))
    {
      bz_backlog_reader_start_document
        (reader, element_name, attribute_names, attribute_values, error);
    }
  else
    {
      g_set_error
        (error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ELEMENT,
         "Unknown document element: %s", element_name);
    }
}

static void
bz_backlog_reader_end_element (GMarkupParseContext *context,
                               const char          *element_name,
                               gpointer             user_data,
                               GError             **error)
{
  BzBacklogEndElementHandler end_element = NULL;
  BzBacklogReader *reader = user_data;
  BzBacklogNode *node = NULL;

  if (reader->end_element_handlers)
    end_element = g_hash_table_lookup (reader->end_element_handlers, element_name);
  if (end_element)
    end_element (reader, element_name, error);

  if (reader->node_stack)
    node = reader->node_stack->data;

  if (node && !strcmp (node->name, element_name))
    {
      reader->node_stack = g_slist_remove (reader->node_stack, node);
      bz_backlog_node_unref (node);
    }
}

static void
bz_backlog_reader_text (GMarkupParseContext *context,
                        const char          *text,
                        gsize                text_len,
                        gpointer             user_data,
                        GError             **error)
{
  BzBacklogReader *reader = user_data;
  BzBacklogNode *node = NULL;

  if (reader->node_stack)
    node = reader->node_stack->data;

  if (node)
    switch (node->type)
      {
        case BZ_BACKLOG_NODE_CELL:
          g_return_if_fail (!node->cell.text);
          node->cell.text = g_strdup (text);
          break;

        case BZ_BACKLOG_NODE_SHEET:
        case BZ_BACKLOG_NODE_STYLE:
        case BZ_BACKLOG_NODE_STYLE_REGION:
        case BZ_BACKLOG_NODE_UNKNOWN:
          break;
      }
}

static BzBacklogReader *
bz_backlog_reader_new (BzBacklogModel *model)
{
  static const GMarkupParser markup_handlers = {
    start_element: bz_backlog_reader_start_element,
    end_element: bz_backlog_reader_end_element,
    text: bz_backlog_reader_text,
  };

  BzBacklogReader *reader;

  reader = g_slice_new0 (BzBacklogReader);
  reader->cancellable = g_cancellable_new ();
  reader->model = g_object_ref (model);
  reader->sheets = g_ptr_array_new ();

#if 0
  reader->cells = g_hash_table_new_full
    (g_str_hash, g_str_equal, g_free, (GDestroyNotify)
      bz_backlog_node_unref);
#endif

  reader->parser = g_markup_parse_context_new
    (&markup_handlers, G_MARKUP_TREAT_CDATA_AS_TEXT |
     G_MARKUP_PREFIX_ERROR_POSITION, reader, NULL);

  return reader;
}

static gboolean
bz_backlog_reader_finish (BzBacklogReader  *reader,
                          GError          **error)
{
  struct {
    const char    *text;
    BzBacklogNode *node;
    gboolean       required;
  } headers[] = {
    [BZ_BACKLOG_MODEL_COLUMN_BUG]         { "Bug",         NULL, TRUE  },
    [BZ_BACKLOG_MODEL_COLUMN_EFFORT]      { "Effort",      NULL, FALSE },
    [BZ_BACKLOG_MODEL_COLUMN_SEVERITY]    { "Severity",    NULL, FALSE },
    [BZ_BACKLOG_MODEL_COLUMN_PRIORITY]    { "Priority",    NULL, FALSE },
    [BZ_BACKLOG_MODEL_COLUMN_KEYWORDS]    { "Keywords",    NULL, FALSE },
    [BZ_BACKLOG_MODEL_COLUMN_DESCRIPTION] { "Description", NULL, TRUE  },
  };

  BzBacklogModelPrivate *priv;
  BzBacklogNode *sheet = NULL;
  BzBacklogNode *bugs = NULL;
  GPtrArray *day_names;
  int i, j;

  if (!g_markup_parse_context_end_parse (reader->parser, error))
    return FALSE;

  priv = GET_PRIVATE (reader->model);

  if (reader != priv->reader)
    return TRUE;

  if (reader->sheets->len > 0)
    sheet = reader->sheets->pdata[0];

  g_return_val_if_fail (NULL != sheet, FALSE);

  day_names = g_ptr_array_new ();

  if (priv->day_names)
    g_strfreev (priv->day_names);

  priv->day_names = NULL;
  priv->columns = 0;

  /* find entry point ("Bug" column) */

  for (i = 0; i < sheet->sheet.cells->len; ++i)
    {
      BzBacklogNode *node = sheet->sheet.cells->pdata[i];

      if (!node || !node->cell.text)
        continue;

      if (!strcmp (node->cell.text, headers[BZ_BACKLOG_MODEL_COLUMN_BUG].text))
        {
          bugs = node;
          break;
        }
    }

  if (!bugs)
    {
      g_set_error
        (error, G_MARKUP_ERROR,
         G_MARKUP_ERROR_INVALID_CONTENT,
         _("Backlog headers not found"));

      goto failure;
    }

  /* find other headers */

  for (i = 0; i < sheet->sheet.n_columns; ++i)
    {
      BzBacklogNode *node = bz_backlog_sheet_get_cell (sheet, bugs->cell.row, i);

      if (!node || !node->cell.text)
        continue;

      if (!strcmp (node->cell.text, "Mon") ||
          !strcmp (node->cell.text, "Tue") ||
          !strcmp (node->cell.text, "Wed") ||
          !strcmp (node->cell.text, "Thu") ||
          !strcmp (node->cell.text, "Fri") ||
          !strcmp (node->cell.text, "Sat") ||
          !strcmp (node->cell.text, "Sun"))
        {
          g_ptr_array_add (day_names, node);
          continue;
        }

      for (j = 0; j < G_N_ELEMENTS (headers); ++j)
        {
          if (strcmp (node->cell.text, headers[j].text))
            continue;

          if (headers[j].node)
            {
              g_set_error
                (error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
                 _("Duplicate backlog header: %s"), headers[j].text);

              goto failure;
            }

          headers[j].node = node;
        }
    }

  /* check if all required headers found */

  for (j = 0; j < G_N_ELEMENTS (headers); ++j)
    if (!headers[j].node && headers[j].required)
      {
        g_set_error
          (error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
           _("Backlog column not found: %s"), headers[j].text);

        goto failure;
      }

  /* read backlog rows */

  for (i = bugs->cell.row + 1;; ++i)
    {
      BzBacklogNode *node = bz_backlog_sheet_get_cell (sheet, i, bugs->cell.col);
      BzBacklogDayArray *days;
      GtkTreeIter iter;

      if (!node)
        break;

      gtk_list_store_append (GTK_LIST_STORE (reader->model), &iter);

      for (j = 0; j < G_N_ELEMENTS (headers); ++j)
        {
          if (!headers[j].node)
            continue;

          node = bz_backlog_sheet_get_cell (sheet, i, headers[j].node->cell.col);

          if (!node)
            continue;

          if (G_TYPE_UINT != gtk_tree_model_get_column_type (GTK_TREE_MODEL (reader->model), j))
            gtk_list_store_set
              (GTK_LIST_STORE (reader->model), &iter, j, node->cell.text, -1);
          else if (node->cell.text)
            gtk_list_store_set
              (GTK_LIST_STORE (reader->model), &iter, j, atoi (node->cell.text), -1);

          priv->columns |= (1 << j);
        }

      days = bz_backlog_day_array_new (day_names->len);

      for (j = 0; j < day_names->len; ++j)
        {
          BzBacklogDay *day;

          node = day_names->pdata[j];
          node = bz_backlog_sheet_get_cell (sheet, i, node->cell.col);

          if (!node)
            continue;

          day = bz_backlog_day_array_get (days, j);
          day->note = g_strdup (node->cell.text);

          if (node->cell.style)
            {
              day->bg = gdk_color_copy (&node->cell.style->style.bg);
              day->fg = gdk_color_copy (&node->cell.style->style.fg);
            }
          else
            g_print ("%s(%d:%d): %s\n", ((BzBacklogNode*) day_names->pdata[j])->cell.text, node->cell.row, node->cell.col, day->note ? day->note : "");
        }

      gtk_list_store_set
        (GTK_LIST_STORE (reader->model), &iter,
         BZ_BACKLOG_MODEL_COLUMN_DAYS, days, -1);

      bz_backlog_day_array_free (days);
    }

  for (i = 0; i < day_names->len; ++i)
    {
      BzBacklogNode *node = day_names->pdata[i];
      day_names->pdata[i] = g_strdup (node->cell.text);
    }

  g_ptr_array_add (day_names, NULL);
  priv->day_names = (char **) g_ptr_array_free (day_names, FALSE);

  return TRUE;

failure:
  g_ptr_array_free (day_names, TRUE);
  return FALSE;
}

static void
bz_backlog_reader_free (BzBacklogReader *reader)
{
  BzBacklogModelPrivate *priv = GET_PRIVATE (reader->model);

  if (priv->reader == reader)
    priv->reader = NULL;

  g_markup_parse_context_free (reader->parser);

  g_slist_foreach (reader->node_stack, (GFunc) bz_backlog_node_unref, NULL);
  g_slist_free (reader->node_stack);

  bz_backlog_node_array_free (reader->sheets);

  if (reader->start_element_handlers)
    g_hash_table_unref (reader->start_element_handlers);
  if (reader->end_element_handlers)
    g_hash_table_unref (reader->end_element_handlers);

  g_object_unref (reader->cancellable);
  g_object_unref (reader->model);

  g_slice_free (BzBacklogReader, reader);
}

static void
bz_backlog_model_init (BzBacklogModel *model)
{
  GType column_types[BZ_BACKLOG_MODEL_N_COLUMNS] = {
    G_TYPE_STRING, G_TYPE_UINT, G_TYPE_STRING, G_TYPE_STRING,
    G_TYPE_STRING, G_TYPE_STRING, BZ_TYPE_BACKLOG_DAY_ARRAY,
  };

  gtk_list_store_set_column_types
    (GTK_LIST_STORE (model), G_N_ELEMENTS (column_types), column_types);
}

#if 0
static void
bz_backlog_model_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  BzBacklogModel *model = BZ_BACKLOG_MODEL (object);

  switch (prop_id)
    {
      /* TODO: handle properties */

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
bz_backlog_model_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  BzBacklogModel *model = BZ_BACKLOG_MODEL (object);

  switch (prop_id)
    {
      /* TODO: handle properties */

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}
#endif

static void
bz_backlog_model_dispose (GObject *object)
{
  bz_backlog_model_cancel (BZ_BACKLOG_MODEL (object));

  DELEGATE (G_OBJECT_CLASS, dispose, object);
}

static void
bz_backlog_model_finalize (GObject *object)
{
  BzBacklogModelPrivate *priv = GET_PRIVATE (object);

  if (priv->day_names)
    g_strfreev (priv->day_names);

  DELEGATE (G_OBJECT_CLASS, finalize, object);
}

static void
bz_backlog_model_class_init (BzBacklogModelClass *cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);

  object_class->dispose  = bz_backlog_model_dispose;
  object_class->finalize = bz_backlog_model_finalize;

  signals[READY] = g_signal_new
    ("ready", BZ_TYPE_BACKLOG_MODEL,
     0, 0, NULL, NULL, g_cclosure_marshal_VOID__VOID,
     G_TYPE_NONE, 0);

  signals[FAILED] = g_signal_new
    ("failed", BZ_TYPE_BACKLOG_MODEL,
     0, 0, NULL, NULL, g_cclosure_marshal_VOID__POINTER,
     G_TYPE_NONE, 1, G_TYPE_POINTER);

  g_type_class_add_private (cls, sizeof (BzBacklogModelPrivate));
}

BzBacklogModel *
bz_backlog_model_new (void)
{
  return g_object_new (BZ_TYPE_BACKLOG_MODEL, NULL);
}

static void
bz_backlog_model_emit_loading_failed (BzBacklogModel *model,
                                      const GError   *error)
{
  if (error)
    g_warning
      ("%s: %s: %s\n", G_STRFUNC,
       g_quark_to_string (error->domain), error->message);

  g_signal_emit (model, signals[FAILED], 0, error);
}

static void
bz_backlog_model_stream_close_cb (GObject      *object,
                                  GAsyncResult *result,
                                  gpointer      user_data)
{
  BzBacklogReader *reader = user_data;
  GInputStream *stream = G_INPUT_STREAM (object);
  GError *error = NULL;

  if (g_input_stream_close_finish (stream, result, &error) &&
      bz_backlog_reader_finish (reader, &error))
    {
      g_signal_emit (reader->model, signals[READY], 0);
    }
  else
    {
      bz_backlog_model_emit_loading_failed (reader->model, error);
      g_error_free (error);
    }

  bz_backlog_reader_free (reader);
  g_object_unref (stream);
}

static void
bz_backlog_model_stream_read_cb (GObject      *object,
                                 GAsyncResult *result,
                                 gpointer      user_data)
{
  BzBacklogReader *reader = user_data;
  GInputStream *stream = G_INPUT_STREAM (object);
  GError *error = NULL;
  gssize length;

  length = g_input_stream_read_finish (stream, result, &error);

  if (length > 0 && g_markup_parse_context_parse
        (reader->parser, reader->buffer, length, &error))
    {
      g_input_stream_read_async
        (G_INPUT_STREAM (stream),
         reader->buffer, sizeof reader->buffer,
         G_PRIORITY_DEFAULT, reader->cancellable,
         bz_backlog_model_stream_read_cb, reader);
    }
  else if (!error)
    {
      g_input_stream_close_async
        (stream, G_PRIORITY_DEFAULT, reader->cancellable,
         bz_backlog_model_stream_close_cb, reader);
    }
  else
    {
      bz_backlog_model_emit_loading_failed (reader->model, error);
      bz_backlog_reader_free (reader);
      g_object_unref (stream);
      g_error_free (error);
    }
}

static void
bz_backlog_model_file_read_cb (GObject      *object,
                               GAsyncResult *result,
                               gpointer      user_data)
{
  BzBacklogReader *reader = user_data;
  GFile *file = G_FILE (object);
  GFileInputStream *stream;
  GError *error = NULL;

  stream = g_file_read_finish (file, result, &error);

  if (stream)
    {
      g_input_stream_read_async
        (G_INPUT_STREAM (stream),
         reader->buffer, sizeof reader->buffer,
         G_PRIORITY_DEFAULT, reader->cancellable,
         bz_backlog_model_stream_read_cb, reader);
    }
  else
    {
      bz_backlog_model_emit_loading_failed (reader->model, error);
      bz_backlog_reader_free (reader);
      g_error_free (error);
    }
}

void
bz_backlog_model_load (BzBacklogModel *model,
                       GFile          *file)
{
  BzBacklogModelPrivate *priv;

  g_return_if_fail (BZ_IS_BACKLOG_MODEL (model));
  g_return_if_fail (G_IS_FILE (file));

  bz_backlog_model_cancel (model);

  priv = GET_PRIVATE (model);
  priv->reader = bz_backlog_reader_new (model);

  g_file_read_async
    (file, G_PRIORITY_DEFAULT, priv->reader->cancellable,
     bz_backlog_model_file_read_cb, priv->reader);
}

void
bz_backlog_model_cancel (BzBacklogModel *model)
{
  BzBacklogModelPrivate *priv;

  g_return_if_fail (BZ_IS_BACKLOG_MODEL (model));

  priv = GET_PRIVATE (model);

  if (priv->reader)
    {
      g_cancellable_cancel (priv->reader->cancellable);
      priv->reader = NULL; /* unref happens in callback */
    }

  gtk_list_store_clear (GTK_LIST_STORE (model));
}

gboolean
bz_backlog_model_has_column (BzBacklogModel      *model,
                             BzBacklogModelColumn column)
{
  g_return_val_if_fail (BZ_IS_BACKLOG_MODEL (model), FALSE);
  g_return_val_if_fail (column < gtk_tree_model_get_n_columns
                              (GTK_TREE_MODEL (model)), FALSE);

  return (0 != (GET_PRIVATE (model)->columns & (1 << column)));
}

const char *const *
bz_backlog_model_get_day_names (BzBacklogModel *model)
{
  g_return_val_if_fail (BZ_IS_BACKLOG_MODEL (model), FALSE);
  return (gpointer) GET_PRIVATE (model)->day_names;
}

