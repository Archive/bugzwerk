#include "config.h"
#include "bz-bugzilla.h"
#include <glib/gi18n.h>

#define GET_PRIVATE(obj) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), \
   BZ_TYPE_BUGZILLA, BzBugzillaPrivate))

#define DELEGATE(class, method, ...) G_STMT_START{ \
  if (class (bz_bugzilla_parent_class)->method) \
    class (bz_bugzilla_parent_class)->method (__VA_ARGS__); \
}G_STMT_END

enum {
  PROP_NONE,
  PROP_URI,
};

typedef struct {
  char         *uri;
  guint64       max_cache_age;
} BzBugzillaPrivate;

G_DEFINE_TYPE (BzBugzilla, bz_bugzilla, G_TYPE_OBJECT);

static void
bz_bugzilla_init (BzBugzilla *bugzilla)
{
  BzBugzillaPrivate *priv = GET_PRIVATE (bugzilla);

  priv->max_cache_age = 7 * 24 * 60 * 60;
}

static void
bz_bugzilla_finalize (GObject *object)
{
  BzBugzillaPrivate *priv = GET_PRIVATE (object);

  g_free (priv->uri);

  DELEGATE (G_OBJECT_CLASS, finalize, object);
}

static void
bz_bugzilla_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  BzBugzillaPrivate *priv = GET_PRIVATE (object);

  switch (prop_id)
    {
      case PROP_URI:
        g_return_if_fail (NULL == priv->uri);
        priv->uri = g_value_dup_string (value);
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
bz_bugzilla_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  BzBugzilla *bugzilla = BZ_BUGZILLA (object);

  switch (prop_id)
    {
      case PROP_URI:
        g_value_set_string (value, bz_bugzilla_get_uri (bugzilla));
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
bz_bugzilla_class_init (BzBugzillaClass *cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);
  GParamSpec *pspec;

  object_class->finalize     = bz_bugzilla_finalize;
  object_class->set_property = bz_bugzilla_set_property;
  object_class->get_property = bz_bugzilla_get_property;

  pspec = g_param_spec_string
    ("uri", _("URI"), _("Base URI of this Bugzilla instance"), NULL,
     G_PARAM_STATIC_STRINGS | G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
  g_object_class_install_property (object_class, PROP_URI, pspec);

  g_type_class_add_private (cls, sizeof (BzBugzillaPrivate));
}

BzBugzilla *
bz_bugzilla_new (const char *uri)
{
  g_return_val_if_fail (NULL != uri, NULL);
  return g_object_new (BZ_TYPE_BUGZILLA, "uri", uri, NULL);
}

const char *
bz_bugzilla_get_uri (BzBugzilla *bugzilla)
{
  g_return_val_if_fail (BZ_IS_BUGZILLA (bugzilla), NULL);
  return GET_PRIVATE (bugzilla)->uri;
}

/*********************************************************************/

static GFile *
bz_bugzilla_get_remote_info_file (BzBugzillaPrivate *priv)
{
  GFile *file;
  char *uri;

  uri = g_strconcat (priv->uri, "/config.cgi?ctype=rdf", NULL);
  file = g_file_new_for_uri (uri);
  g_free (uri);

  return file;
}

static GFile *
bz_bugzilla_get_local_info_file (BzBugzilla *bugzilla)
{
  char  *hash, *name, *path;
  GFile *file;

  hash = g_compute_checksum_for_string
    (G_CHECKSUM_MD5, bz_bugzilla_get_uri (bugzilla), -1);

  name = g_strconcat (hash, ".config", NULL);
  path = g_build_filename (g_get_user_cache_dir (), PACKAGE, name, NULL);
  file = g_file_new_for_path (path);

  g_free (path);
  g_free (name);
  g_free (hash);

  return file;
}

BzBugzillaInfo *
bz_bugzilla_query_info (BzBugzilla    *bugzilla,
                        GCancellable  *cancellable,
                        GError       **error)
{
  BzBugzillaInfo *info = NULL;
  BzBugzillaPrivate *priv;
  char *contents;
  gsize length;
  GFile *file;

  g_return_val_if_fail (BZ_IS_BUGZILLA (bugzilla), FALSE);
  g_return_val_if_fail (G_IS_CANCELLABLE (cancellable) || !cancellable, FALSE);

  priv = GET_PRIVATE (bugzilla);

  file = bz_bugzilla_get_remote_info_file (priv);

  if (g_file_load_contents (file, cancellable, &contents, &length, NULL, error))
    info = bz_bugzilla_info_new_from_xml (contents, length, error);

  g_object_unref (file);

  return info;
}

static void
bz_bugzilla_query_info_load_cb (GObject      *object,
                                GAsyncResult *result,
                                gpointer      user_data)
{
  BzBugzillaInfo *info = NULL;
  char *contents = NULL;
  GError *error = NULL;
  gsize length;

  if (g_file_load_contents_finish (G_FILE (object), result, &contents, &length, NULL, &error))
    info = bz_bugzilla_info_new_from_xml (contents, length, &error);

  if (info)
    g_simple_async_result_set_op_res_gpointer (user_data, info, g_object_unref);
  else
    g_simple_async_result_set_from_error (user_data, error);

  g_clear_error (&error);
  g_free (contents);

  g_simple_async_result_complete_in_idle (user_data);
}

static void
bz_bugzilla_query_info_copy_cb (GObject      *object,
                                GAsyncResult *result,
                                gpointer      user_data)
{
  GError *error = NULL;

  if (g_file_copy_finish (G_FILE (object), result, &error))
    {
      BzBugzilla *bugzilla = BZ_BUGZILLA (g_async_result_get_source_object (user_data));
      GCancellable *cancellable = g_object_get_data (user_data, "cancellable");
      GFile *local_file = bz_bugzilla_get_local_info_file (bugzilla);

      g_file_load_contents_async
        (local_file, cancellable, bz_bugzilla_query_info_load_cb, user_data);

      g_object_unref (local_file);
    }
  else
    {
      g_simple_async_result_set_from_error (user_data, error);
      g_simple_async_result_complete_in_idle (user_data);

      g_error_free (error);
    }
}

static void
bz_bugzilla_query_info_async_query_info_cb (GObject      *object,
                                            GAsyncResult *result,
                                            gpointer      user_data)
{
  BzBugzilla *bugzilla = BZ_BUGZILLA (g_async_result_get_source_object (user_data));
  GCancellable *cancellable = g_object_get_data (user_data, "cancellable");
  BzBugzillaPrivate *priv = GET_PRIVATE (bugzilla);
  GFile *local_file = G_FILE (object);
  guint64 last_modified = 0;
  GError *error = NULL;
  GFileInfo *info;

  info = g_file_query_info_finish (local_file, result, &error);

  if (info)
    {
      if (g_file_info_get_attribute_boolean (info, G_FILE_ATTRIBUTE_ACCESS_CAN_READ))
        last_modified = g_file_info_get_attribute_uint64 (info, G_FILE_ATTRIBUTE_TIME_MODIFIED);

      g_object_unref (info);
    }

  if (last_modified + priv->max_cache_age > time (NULL))
    g_file_load_contents_async
      (local_file, cancellable, bz_bugzilla_query_info_load_cb, user_data);
  else
    {
      GFile *remote_file = bz_bugzilla_get_remote_info_file (priv);

      g_file_copy_async
        (remote_file, local_file, G_FILE_COPY_OVERWRITE, G_PRIORITY_DEFAULT,
         cancellable, NULL, NULL, bz_bugzilla_query_info_copy_cb, user_data);

      g_object_unref (remote_file);
    }
}

void
bz_bugzilla_query_info_async (BzBugzilla          *bugzilla,
                              GCancellable        *cancellable,
                              GAsyncReadyCallback  callback,
                              gpointer             user_data)
{
g_print ("%s\n", G_STRFUNC);
  GSimpleAsyncResult *simple_result;
  GFile *file;

  g_return_if_fail (BZ_IS_BUGZILLA (bugzilla));
  g_return_if_fail (G_IS_CANCELLABLE (cancellable) || !cancellable);

  simple_result = g_simple_async_result_new
    (G_OBJECT (bugzilla), callback, user_data,
     bz_bugzilla_query_info_async);

  if (cancellable)
    {
      g_object_set_data_full
        (G_OBJECT (simple_result), "cancellable",
         g_object_ref (cancellable), g_object_unref);
    }

  file = bz_bugzilla_get_local_info_file (bugzilla);

  g_file_query_info_async
    (file, G_FILE_ATTRIBUTE_TIME_MODIFIED "," G_FILE_ATTRIBUTE_ACCESS_CAN_READ,
     G_FILE_QUERY_INFO_NONE, G_PRIORITY_DEFAULT, cancellable,
     bz_bugzilla_query_info_async_query_info_cb, simple_result);

  g_object_unref (file);
}

BzBugzillaInfo *
bz_bugzilla_query_info_finish (BzBugzilla    *bugzilla,
                               GAsyncResult  *result,
                               GError       **error)
{
  GSimpleAsyncResult *simple_result;
  BzBugzillaInfo *info = NULL;
  gpointer source_tag;

  g_return_val_if_fail (BZ_IS_BUGZILLA (bugzilla), FALSE);
  g_return_val_if_fail (G_IS_SIMPLE_ASYNC_RESULT (result), FALSE);

  simple_result = G_SIMPLE_ASYNC_RESULT (result);
  source_tag = g_simple_async_result_get_source_tag (simple_result);

  g_return_val_if_fail (bz_bugzilla_query_info_async == source_tag, FALSE);

  if (!g_simple_async_result_propagate_error (simple_result, error))
    info = g_simple_async_result_get_op_res_gpointer (simple_result);

  if (info)
    g_object_ref (info);

  g_object_unref (result);

  return info;
}

/*********************************************************************/

static GFile *
bz_bugzilla_get_bug_file (BzBugzilla *bugzilla,
                          const char *bugno)
{
  GFile *file;
  char *uri;

  uri = g_strconcat
    (bz_bugzilla_get_uri (bugzilla),
     "/show_bug.cgi?ctype=xml&excludefield=attachmentdata&id=",
     bugno, NULL);

  file = g_file_new_for_uri (uri);
  g_free (uri);

  return file;
}

BzBugzillaBug *
bz_bugzilla_lookup_bug (BzBugzilla    *bugzilla,
                        const char    *bugno,
                        GCancellable  *cancellable,
                        GError       **error)
{
  BzBugzillaBug *bug = NULL;
  char *contents;
  gsize length;
  GFile *file;

  g_return_val_if_fail (BZ_IS_BUGZILLA (bugzilla), NULL);
  g_return_val_if_fail (G_IS_CANCELLABLE (cancellable) || !cancellable, NULL);
  g_return_val_if_fail (NULL != bugno, NULL);

  file = bz_bugzilla_get_bug_file (bugzilla, bugno);

  if (g_file_load_contents (file, cancellable, &contents, &length, NULL, error))
    {
      bug = bz_bugzilla_bug_new_from_xml (contents, length, error);
      g_free (contents);
    }

  g_object_unref (file);

  return bug;
}

static void
bz_bugzilla_lookup_bug_cb (GObject      *object,
                           GAsyncResult *result,
                           gpointer      user_data)
{
  GSimpleAsyncResult *simple_result = user_data;
  GFile *file = G_FILE (object);
  BzBugzillaBug *bug = NULL;
  char *contents = NULL;
  GError *error = NULL;
  gsize length;

  if (g_file_load_contents_finish (file, result, &contents, &length, NULL, &error))
    bug = bz_bugzilla_bug_new_from_xml (contents, length, &error);

  if (bug)
    g_simple_async_result_set_op_res_gpointer (simple_result, bug, g_object_unref);
  else
    g_simple_async_result_set_from_error (simple_result, error);

  g_clear_error (&error);
  g_free (contents);

  g_simple_async_result_complete_in_idle (simple_result);
}

void
bz_bugzilla_lookup_bug_async (BzBugzilla          *bugzilla,
                              const char          *bugno,
                              GCancellable        *cancellable,
                              GAsyncReadyCallback  callback,
                              gpointer             user_data)
{
  GSimpleAsyncResult *result;
  GFile *file;

  g_return_if_fail (BZ_IS_BUGZILLA (bugzilla));
  g_return_if_fail (G_IS_CANCELLABLE (cancellable) || !cancellable);
  g_return_if_fail (NULL != bugno);

  file = bz_bugzilla_get_bug_file (bugzilla, bugno);

  result = g_simple_async_result_new
    (G_OBJECT (bugzilla), callback, user_data,
     bz_bugzilla_lookup_bug_async);
  g_file_load_contents_async
    (file, cancellable, bz_bugzilla_lookup_bug_cb, result);

  g_object_unref (file);
}

BzBugzillaBug *
bz_bugzilla_lookup_bug_finish (BzBugzilla    *bugzilla,
                               GAsyncResult  *result,
                               GError       **error)
{
  GSimpleAsyncResult *simple_result;
  BzBugzillaBug *bug = NULL;
  gpointer source_tag;

  g_return_val_if_fail (BZ_IS_BUGZILLA (bugzilla), NULL);
  g_return_val_if_fail (G_IS_SIMPLE_ASYNC_RESULT (result), NULL);

  simple_result = G_SIMPLE_ASYNC_RESULT (result);
  source_tag = g_simple_async_result_get_source_tag (simple_result);

  g_return_val_if_fail (bz_bugzilla_lookup_bug_async == source_tag, NULL);

  if (!g_simple_async_result_propagate_error (simple_result, error))
    bug = g_simple_async_result_get_op_res_gpointer (simple_result);

  if (bug)
    g_object_ref (bug);

  g_object_unref (result);

  return bug;
}
