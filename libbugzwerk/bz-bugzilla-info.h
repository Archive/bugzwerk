#ifndef __BZ_BUGZILLA_INFO_H__
#define __BZ_BUGZILLA_INFO_H__

#include <gio/gio.h>

G_BEGIN_DECLS

#define BZ_TYPE_BACKLOG_DAY_ARRAY \
  (bz_backlog_day_array_get_type ())

#define BZ_TYPE_BUGZILLA_INFO \
  (bz_bugzilla_info_get_type ())
#define BZ_BUGZILLA_INFO(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
   BZ_TYPE_BUGZILLA_INFO, BzBugzillaInfo))
#define BZ_BUGZILLA_INFO_CLASS(cls) \
  (G_TYPE_CHECK_CLASS_CAST ((cls), \
   BZ_TYPE_BUGZILLA_INFO, BzBugzillaInfoClass))
#define BZ_IS_BUGZILLA_INFO(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
   BZ_TYPE_BUGZILLA_INFO))
#define BZ_IS_BUGZILLA_INFO_CLASS(obj) \
  (G_TYPE_CHECK_CLASS_TYPE ((obj), \
   BZ_TYPE_BUGZILLA_INFO))
#define BZ_BUGZILLA_INFO_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
   BZ_TYPE_BUGZILLA_INFO, BzBugzillaInfoClass))

typedef struct _BzBugzillaInfo      BzBugzillaInfo;
typedef struct _BzBugzillaInfoClass BzBugzillaInfoClass;

struct _BzBugzillaInfo {
  GObject parent_instance;
};

struct _BzBugzillaInfoClass {
  GObjectClass parent_class;
};

GType
bz_bugzilla_info_get_type        (void) G_GNUC_CONST;

BzBugzillaInfo *
bz_bugzilla_info_new_from_xml    (const char     *contents,
                                  gssize          length,
                                  GError        **error);

const char *
bz_bugzilla_info_get_string      (BzBugzillaInfo *info,
                                  const char     *name);

const char **
bz_bugzilla_info_get_string_list (BzBugzillaInfo *info,
                                  const char     *name);

G_END_DECLS

#endif /* __BZ_BUGZILLA_INFO_H__ */

