#ifndef __BZ_BACKLOG_VIEW_H__
#define __BZ_BACKLOG_VIEW_H__

#include "bz-backlog-model.h"

G_BEGIN_DECLS

#define BZ_TYPE_BACKLOG_VIEW \
  (bz_backlog_view_get_type ())
#define BZ_BACKLOG_VIEW(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
   BZ_TYPE_BACKLOG_VIEW, BzBacklogView))
#define BZ_BACKLOG_VIEW_CLASS(cls) \
  (G_TYPE_CHECK_CLASS_CAST ((cls), \
   BZ_TYPE_BACKLOG_VIEW, BzBacklogViewClass))
#define BZ_IS_BACKLOG_VIEW(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
   BZ_TYPE_BACKLOG_VIEW))
#define BZ_IS_BACKLOG_VIEW_CLASS(obj) \
  (G_TYPE_CHECK_CLASS_TYPE ((obj), \
   BZ_TYPE_BACKLOG_VIEW))
#define BZ_BACKLOG_VIEW_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
   BZ_TYPE_BACKLOG_VIEW, BzBacklogViewClass))

typedef struct _BzBacklogView      BzBacklogView;
typedef struct _BzBacklogViewClass BzBacklogViewClass;

struct _BzBacklogView {
  GtkTreeView parent_instance;
};

struct _BzBacklogViewClass {
  GtkTreeViewClass parent_class;
};

GType
bz_backlog_view_get_type       (void) G_GNUC_CONST;

GtkWidget *
bz_backlog_view_new            (void);

GtkWidget *
bz_backlog_view_new_with_model (BzBacklogModel *model);

G_END_DECLS

#endif /* __BZ_BACKLOG_VIEW_H__ */
