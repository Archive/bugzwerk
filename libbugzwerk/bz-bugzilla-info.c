#include "config.h"
#include "bz-bugzilla-info.h"
#include <string.h>

#define GET_PRIVATE(obj) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), \
   BZ_TYPE_BUGZILLA_INFO, BzBugzillaInfoPrivate))

#define DELEGATE(class, method, ...) G_STMT_START{ \
  if (class (bz_bugzilla_info_parent_class)->method) \
    class (bz_bugzilla_info_parent_class)->method (__VA_ARGS__); \
}G_STMT_END

typedef enum {
  BZ_BUGZILLA_INFO_ATTRIBUTE_STRING,
  BZ_BUGZILLA_INFO_ATTRIBUTE_STRING_LIST,
} BzBugzillaInfoAttributeType;

typedef struct {
  GHashTable *attributes;
} BzBugzillaInfoPrivate;

typedef struct {
  BzBugzillaInfoAttributeType type;

  union {
    char      *string;
    GPtrArray *string_list;
  } data;
} BzBugzillaInfoAttribute;

G_DEFINE_TYPE (BzBugzillaInfo, bz_bugzilla_info, G_TYPE_OBJECT);

static BzBugzillaInfoAttribute *
bz_bugzilla_info_attribute_new (BzBugzillaInfoAttributeType type)
{
  BzBugzillaInfoAttribute *attr;

  attr = g_slice_new0 (BzBugzillaInfoAttribute);
  attr->type = type;

  switch (type)
    {
      case BZ_BUGZILLA_INFO_ATTRIBUTE_STRING:
        break;

      case BZ_BUGZILLA_INFO_ATTRIBUTE_STRING_LIST:
        attr->data.string_list = g_ptr_array_new ();
        break;
    }

  return attr;
}

static void
bz_bugzilla_info_attribute_free (gpointer data)
{
  BzBugzillaInfoAttribute *attr= data;

  switch (attr->type)
  {
    case BZ_BUGZILLA_INFO_ATTRIBUTE_STRING:
      g_free (attr->data.string);
      break;

    case BZ_BUGZILLA_INFO_ATTRIBUTE_STRING_LIST:
      g_ptr_array_foreach (attr->data.string_list, (GFunc) g_free, NULL);
      g_ptr_array_free (attr->data.string_list, TRUE);
      break;
  }

  g_slice_free (BzBugzillaInfoAttribute, attr);
}

void
bz_bugzilla_info_init (BzBugzillaInfo *info)
{
  BzBugzillaInfoPrivate *priv = GET_PRIVATE (info);

  priv->attributes = g_hash_table_new_full
    (g_str_hash, g_str_equal, g_free,
     bz_bugzilla_info_attribute_free);
}

static void
bz_bugzilla_info_finalize (GObject *object)
{
  BzBugzillaInfoPrivate *priv = GET_PRIVATE (object);

  g_hash_table_unref (priv->attributes);

  DELEGATE (G_OBJECT_CLASS, finalize, object);
}

void
bz_bugzilla_info_class_init (BzBugzillaInfoClass *cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);

  object_class->finalize = bz_bugzilla_info_finalize;

  g_type_class_add_private (cls, sizeof (BzBugzillaInfoPrivate));
}

static const char *
bz_bugzilla_info_axis_match (const GSList *stack,
                             const char   *axis[],
                             gsize         length)
{
  const char *name = NULL;
  int i;

  for (i = 0; i < length; ++i)
    {
      if (!stack || (axis[i] && strcmp (stack->data, axis[i])))
        break;

      if (!axis[i])
        name = stack->data;

      stack = g_slist_next (stack);
    }

  if (stack)
    name = NULL;

  return name;
}

static void
bz_bugzilla_info_text (GMarkupParseContext *context,
                       const char          *text,
                       gsize                text_len,
                       gpointer             user_data,
                       GError             **error)
{
  static const char *axis[] = { "li", "Seq", NULL, "bz:installation", "RDF" };
  BzBugzillaInfoPrivate *priv = GET_PRIVATE (user_data);
  BzBugzillaInfoAttributeType type;
  BzBugzillaInfoAttribute *attr;
  const char *name = NULL;
  const GSList *stack;

  stack = g_markup_parse_context_get_element_stack (context);

  if (g_ascii_isspace (*text))
    return;

  if (NULL != (name = bz_bugzilla_info_axis_match (stack, axis, G_N_ELEMENTS (axis))))
    type = BZ_BUGZILLA_INFO_ATTRIBUTE_STRING_LIST;
  else if (NULL != (name = bz_bugzilla_info_axis_match (stack, axis + 2, G_N_ELEMENTS (axis) - 2)))
    type = BZ_BUGZILLA_INFO_ATTRIBUTE_STRING;
  else
    return;

  attr = g_hash_table_lookup (priv->attributes, name);

  if (!attr)
    {
      attr = bz_bugzilla_info_attribute_new (type);
      g_hash_table_insert (priv->attributes, g_strdup (name), attr);
    }

  switch (type)
    {
      case BZ_BUGZILLA_INFO_ATTRIBUTE_STRING:
        g_return_if_fail (NULL == attr->data.string);
        attr->data.string = g_strdup (text);
        break;

      case BZ_BUGZILLA_INFO_ATTRIBUTE_STRING_LIST:
        g_ptr_array_add (attr->data.string_list, g_strdup (text));
        break;
    }
}

BzBugzillaInfo *
bz_bugzilla_info_new_from_xml (const char *contents,
                               gssize      length,
                               GError    **error)
{
  static const GMarkupParser markup_handlers = {
    text: bz_bugzilla_info_text,
  };

  GMarkupParseContext *context;
  BzBugzillaInfo *info;

  g_return_val_if_fail (NULL != contents, NULL);

  info = g_object_new (BZ_TYPE_BUGZILLA_INFO, NULL);

  context = g_markup_parse_context_new
    (&markup_handlers, G_MARKUP_TREAT_CDATA_AS_TEXT |
     G_MARKUP_PREFIX_ERROR_POSITION, info, NULL);

  if (g_markup_parse_context_parse (context, contents, length, error) &&
      g_markup_parse_context_end_parse (context, error))
    {
      BzBugzillaInfoPrivate *priv = GET_PRIVATE (info);
      BzBugzillaInfoAttribute *attr;
      GHashTableIter iter;

      g_hash_table_iter_init (&iter, priv->attributes);

      while (g_hash_table_iter_next (&iter, NULL, (gpointer) &attr))
        {
          switch (attr->type)
            {
              case BZ_BUGZILLA_INFO_ATTRIBUTE_STRING:
                break;

              case BZ_BUGZILLA_INFO_ATTRIBUTE_STRING_LIST:
                g_ptr_array_add (attr->data.string_list, NULL);
                break;
            }
        }
    }
  else
    {
      g_object_unref (info);
      info = NULL;
    }

  g_markup_parse_context_free (context);

  return info;
}

const char *
bz_bugzilla_info_get_string (BzBugzillaInfo *info,
                             const char     *name)
{
  BzBugzillaInfoAttribute *attr;
  BzBugzillaInfoPrivate *priv;

  g_return_val_if_fail (BZ_IS_BUGZILLA_INFO (info), NULL);
  g_return_val_if_fail (NULL != name, NULL);

  priv = GET_PRIVATE (info);

  attr = g_hash_table_lookup (priv->attributes, name);

  if (attr && BZ_BUGZILLA_INFO_ATTRIBUTE_STRING == attr->type)
    return attr->data.string;

  return NULL;
}

const char **
bz_bugzilla_info_get_string_list (BzBugzillaInfo *info,
                                  const char     *name)
{
  BzBugzillaInfoAttribute *attr;
  BzBugzillaInfoPrivate *priv;

  g_return_val_if_fail (BZ_IS_BUGZILLA_INFO (info), NULL);
  g_return_val_if_fail (NULL != name, NULL);

  priv = GET_PRIVATE (info);

  attr = g_hash_table_lookup (priv->attributes, name);

  if (attr && BZ_BUGZILLA_INFO_ATTRIBUTE_STRING_LIST == attr->type)
    return (gpointer) attr->data.string_list->pdata;

  return NULL;
}
