#ifndef __BZ_BUGZILLA_H__
#define __BZ_BUGZILLA_H__

#include "bz-bugzilla-bug.h"
#include "bz-bugzilla-info.h"

G_BEGIN_DECLS

#define BZ_TYPE_BACKLOG_DAY_ARRAY \
  (bz_backlog_day_array_get_type ())

#define BZ_TYPE_BUGZILLA \
  (bz_bugzilla_get_type ())
#define BZ_BUGZILLA(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
   BZ_TYPE_BUGZILLA, BzBugzilla))
#define BZ_BUGZILLA_CLASS(cls) \
  (G_TYPE_CHECK_CLASS_CAST ((cls), \
   BZ_TYPE_BUGZILLA, BzBugzillaClass))
#define BZ_IS_BUGZILLA(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
   BZ_TYPE_BUGZILLA))
#define BZ_IS_BUGZILLA_CLASS(obj) \
  (G_TYPE_CHECK_CLASS_TYPE ((obj), \
   BZ_TYPE_BUGZILLA))
#define BZ_BUGZILLA_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
   BZ_TYPE_BUGZILLA, BzBugzillaClass))

typedef struct _BzBugzilla      BzBugzilla;
typedef struct _BzBugzillaClass BzBugzillaClass;

struct _BzBugzilla {
  GObject parent_instance;
};

struct _BzBugzillaClass {
  GObjectClass parent_class;
};

GType
bz_bugzilla_get_type          (void) G_GNUC_CONST;

BzBugzilla *
bz_bugzilla_new               (const char         *uri);

const char *
bz_bugzilla_get_uri           (BzBugzilla         *bugzilla);


BzBugzillaInfo *
bz_bugzilla_query_info        (BzBugzilla         *bugzilla,
                               GCancellable       *cancellable,
                               GError            **error);

void
bz_bugzilla_query_info_async  (BzBugzilla         *bugzilla,
                               GCancellable       *cancellable,
                               GAsyncReadyCallback callback,
                               gpointer            user_data);

BzBugzillaInfo *
bz_bugzilla_query_info_finish (BzBugzilla         *bugzilla,
                               GAsyncResult       *result,
                               GError            **error);

BzBugzillaBug *
bz_bugzilla_lookup_bug        (BzBugzilla         *bugzilla,
                               const char         *bugno,
                               GCancellable       *cancellable,
                               GError            **error);

void
bz_bugzilla_lookup_bug_async  (BzBugzilla         *bugzilla,
                               const char         *bugno,
                               GCancellable       *cancellable,
                               GAsyncReadyCallback callback,
                               gpointer            user_data);

BzBugzillaBug *
bz_bugzilla_lookup_bug_finish (BzBugzilla         *bugzilla,
                               GAsyncResult       *result,
                               GError            **error);

G_END_DECLS

#endif /* __BZ_BUGZILLA_H__ */

