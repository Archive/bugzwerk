#ifndef __BZ_BACKLOG_MODEL_H__
#define __BZ_BACKLOG_MODEL_H__

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define BZ_TYPE_BACKLOG_DAY_ARRAY \
  (bz_backlog_day_array_get_type ())

#define BZ_TYPE_BACKLOG_MODEL \
  (bz_backlog_model_get_type ())
#define BZ_BACKLOG_MODEL(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
   BZ_TYPE_BACKLOG_MODEL, BzBacklogModel))
#define BZ_BACKLOG_MODEL_CLASS(cls) \
  (G_TYPE_CHECK_CLASS_CAST ((cls), \
   BZ_TYPE_BACKLOG_MODEL, BzBacklogModelClass))
#define BZ_IS_BACKLOG_MODEL(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
   BZ_TYPE_BACKLOG_MODEL))
#define BZ_IS_BACKLOG_MODEL_CLASS(obj) \
  (G_TYPE_CHECK_CLASS_TYPE ((obj), \
   BZ_TYPE_BACKLOG_MODEL))
#define BZ_BACKLOG_MODEL_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
   BZ_TYPE_BACKLOG_MODEL, BzBacklogModelClass))

typedef enum {
  BZ_BACKLOG_MODEL_COLUMN_BUG,
  BZ_BACKLOG_MODEL_COLUMN_EFFORT,
  BZ_BACKLOG_MODEL_COLUMN_SEVERITY,
  BZ_BACKLOG_MODEL_COLUMN_PRIORITY,
  BZ_BACKLOG_MODEL_COLUMN_KEYWORDS,
  BZ_BACKLOG_MODEL_COLUMN_DESCRIPTION,
  BZ_BACKLOG_MODEL_COLUMN_DAYS,
  BZ_BACKLOG_MODEL_N_COLUMNS /*<skip>*/
} BzBacklogModelColumn;

typedef struct _BzBacklogDay        BzBacklogDay;
typedef GArray                      BzBacklogDayArray;

typedef struct _BzBacklogModel      BzBacklogModel;
typedef struct _BzBacklogModelClass BzBacklogModelClass;

struct _BzBacklogDay {
  char     *note;
  GdkColor *bg;
  GdkColor *fg;
};

struct _BzBacklogModel {
  GtkListStore parent_instance;
};

struct _BzBacklogModelClass {
  GtkListStoreClass parent_class;
};


GType
bz_backlog_day_array_get_type  (void) G_GNUC_CONST;

BzBacklogDayArray *
bz_backlog_day_array_new       (int                  size);

BzBacklogDayArray *
bz_backlog_day_array_copy      (BzBacklogDayArray   *array);

void
bz_backlog_day_array_free      (BzBacklogDayArray *array);

BzBacklogDay *
bz_backlog_day_array_get       (BzBacklogDayArray   *array,
                                unsigned             i);


GType
bz_backlog_model_get_type      (void) G_GNUC_CONST;

BzBacklogModel *
bz_backlog_model_new           (void);

void
bz_backlog_model_load          (BzBacklogModel      *model,
                                GFile               *file);

void
bz_backlog_model_cancel        (BzBacklogModel      *model);

gboolean
bz_backlog_model_has_column    (BzBacklogModel      *model,
                                BzBacklogModelColumn column);

const char *const *
bz_backlog_model_get_day_names (BzBacklogModel      *model);

G_END_DECLS

#endif /* __BZ_BACKLOG_MODEL_H__ */
