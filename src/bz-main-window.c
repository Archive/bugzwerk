#include "config.h"
#include "bz-main-window.h"

#define GET_PRIVATE(obj) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), \
   BZ_TYPE_MAIN_WINDOW, BzMainWindowPrivate))

#define DELEGATE(class, method, ...) G_STMT_START{ \
  if (class (bz_main_window_parent_class)->method) \
    class (bz_main_window_parent_class)->method (__VA_ARGS__); \
}G_STMT_END

enum {
  PROP_NONE,
  /* TODO: fill in properties */
};

typedef struct {
  GtkWidget *backlog_view;
} BzMainWindowPrivate;

G_DEFINE_TYPE
  (BzMainWindow, bz_main_window,
   GTK_TYPE_WINDOW);

static void
bz_main_window_init (BzMainWindow *window)
{
}

static void
bz_main_window_constructed (GObject *object)
{
  BzMainWindowPrivate *priv = GET_PRIVATE (object);
  GtkWidget           *sw;

  DELEGATE (G_OBJECT_CLASS, constructed, object);

  gtk_window_set_default_size (GTK_WINDOW (object), 800, 500);

  sw = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy
    (GTK_SCROLLED_WINDOW (sw), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_shadow_type
    (GTK_SCROLLED_WINDOW (sw), GTK_SHADOW_ETCHED_IN);
  gtk_container_add (GTK_CONTAINER (object), sw);

  priv->backlog_view = bz_backlog_view_new ();
  gtk_container_add (GTK_CONTAINER (sw), priv->backlog_view);

  gtk_widget_show_all (gtk_bin_get_child (GTK_BIN (object)));
}

#if 0
static void
bz_main_window_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  BzMainWindow *window = BZ_MAIN_WINDOW (object);

  switch (prop_id)
    {
      /* TODO: handle properties */

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
bz_main_window_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  BzMainWindow *window = BZ_MAIN_WINDOW (object);

  switch (prop_id)
    {
      /* TODO: handle properties */

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
bz_main_window_finalize (GObject *object)
{
  BzMainWindow *window = BZ_MAIN_WINDOW (object);

  /* TODO: release resources */

  G_OBJECT_CLASS (bz_main_window_parent_class)->finalize (object);
}

static void
bz_main_window_dispose (GObject *object)
{
  BzMainWindowPrivate *priv = GET_PRIVATE (object);

  if (priv->model)
    priv->model = (g_object_unref (priv->model), NULL);

  DELEGATE (G_OBJECT_CLASS, dispose, object);
}
#endif

static void
bz_main_window_class_init (BzMainWindowClass *cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);

  object_class->constructed = bz_main_window_constructed;
#if 0
  oclass->set_property = bz_main_window_set_property;
  oclass->get_property = bz_main_window_get_property;
  object_class->dispose     = bz_main_window_dispose;
  oclass->finalize = bz_main_window_finalize;
#endif
  
  g_type_class_add_private (cls, sizeof (BzMainWindowPrivate));
}

GtkWidget *
bz_main_window_new (void)
{
  return g_object_new (BZ_TYPE_MAIN_WINDOW, NULL);
}

BzBacklogModel *
bz_main_window_get_backlog_model (BzMainWindow *window)
{
  BzMainWindowPrivate *priv;
  GtkTreeModel        *model;

  g_return_val_if_fail (BZ_IS_MAIN_WINDOW (window), NULL);

  priv = GET_PRIVATE (window);
  model = gtk_tree_view_get_model (GTK_TREE_VIEW (priv->backlog_view));

  return BZ_BACKLOG_MODEL (model);
}

