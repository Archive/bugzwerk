#include "config.h"

#include "bz-main-window.h"

#include <glib/gi18n.h>
#include <locale.h>

int
main (int   argc,
      char *argv[])
{
  GtkWidget      *window;
  BzBacklogModel *model;

  setlocale (LC_ALL, "");
  bindtextdomain (PACKAGE, LOCALEDIR);
  textdomain (PACKAGE);

  gtk_init (&argc, &argv);

  window = bz_main_window_new ();
  model = bz_main_window_get_backlog_model (BZ_MAIN_WINDOW (window));
  g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit), NULL);
  gtk_widget_show (window);

  if (argc > 1)
    {
      char *schema = g_uri_parse_scheme (argv[1]);
      GFile *file;

      if (schema)
        file = g_file_new_for_uri (argv[1]);
      else
        file = g_file_new_for_path (argv[1]);

      bz_backlog_model_load (model, file);

      g_object_unref (file);
      g_free (schema);
    }

  gtk_main ();

  return 0;
}
