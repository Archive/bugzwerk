#ifndef __BZ_MAIN_WINDOW_H__
#define __BZ_MAIN_WINDOW_H__

#include <libbugzwerk/bz.h>

G_BEGIN_DECLS

#define BZ_TYPE_MAIN_WINDOW \
  (bz_main_window_get_type ())
#define BZ_MAIN_WINDOW(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
   BZ_TYPE_MAIN_WINDOW, BzMainWindow))
#define BZ_MAIN_WINDOW_CLASS(cls) \
  (G_TYPE_CHECK_CLASS_CAST ((cls), \
   BZ_TYPE_MAIN_WINDOW, BzMainWindowClass))
#define BZ_IS_MAIN_WINDOW(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
   BZ_TYPE_MAIN_WINDOW))
#define BZ_IS_MAIN_WINDOW_CLASS(obj) \
  (G_TYPE_CHECK_CLASS_TYPE ((obj), \
   BZ_TYPE_MAIN_WINDOW))
#define BZ_MAIN_WINDOW_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
   BZ_TYPE_MAIN_WINDOW, BzMainWindowClass))

typedef struct _BzMainWindow      BzMainWindow;
typedef struct _BzMainWindowClass BzMainWindowClass;

struct _BzMainWindow {
  GtkWindow parent_instance;
};

struct _BzMainWindowClass {
  GtkWindowClass parent_class;
};

GType
bz_main_window_get_type          (void) G_GNUC_CONST;

GtkWidget *
bz_main_window_new               (void);

BzBacklogModel *
bz_main_window_get_backlog_model (BzMainWindow *window);

G_END_DECLS

#endif /* __BZ_MAIN_WINDOW_H__ */
